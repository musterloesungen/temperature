```java
public class TemperatureConverter {
    public static void main(String[] args) {
        // Eingabe für die Umrechnung (Beispielwerte)
        double celsius = 100.0;
        double fahrenheit = 212.0;

        // Ausgabe der Umrechnungsergebnisse
        System.out.println("100 Grad Celsius sind " + celsiusToFahrenheit(celsius) + " Grad Fahrenheit.");
        System.out.println("212 Grad Fahrenheit sind " + fahrenheitToCelsius(fahrenheit) + " Grad Celsius.");
    }

    public static double celsiusToFahrenheit(double celsius) {
        return (celsius * 9/5) + 32;
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        return (fahrenheit - 32) * 5/9;
    }
}
```

In dieser Musterlösung wird eine Klasse `TemperatureConverter` definiert, die eine Hauptfunktion (`main`) und zwei Hilfsfunktionen (`celsiusToFahrenheit` und `fahrenheitToCelsius`) enthält. Die Hauptfunktion ruft die beiden Hilfsfunktionen mit Beispielwerten auf und gibt die Ergebnisse der Umrechnungen aus. Die Hilfsfunktionen führen die eigentlichen Umrechnungen durch.
