### Aufgabe: Temperaturkonverter

#### Teil 1: Prozedurale Programmierung

**Ziel**: Erstelle ein Java-Programm, das Temperaturen von Celsius in Fahrenheit und umgekehrt umrechnet. Das Programm soll die folgenden Funktionen haben:

1. Eine Funktion `celsiusToFahrenheit(double celsius)`, die eine Temperatur in Celsius entgegennimmt und den Wert in Fahrenheit zurückgibt.
2. Eine Funktion `fahrenheitToCelsius(double fahrenheit)`, die eine Temperatur in Fahrenheit entgegennimmt und den Wert in Celsius zurückgibt.
3. Eine Hauptfunktion (`main`), die die Benutzerinteraktion verwaltet. Es soll den Benutzer fragen, welche Art der Umrechnung er durchführen möchte und dann die Eingabetemperatur entgegennehmen.

```java
public class TemperatureConverter {
    public static void main(String[] args) {
        // Hauptfunktion: Benutzerinteraktion
    }

    public static double celsiusToFahrenheit(double celsius) {
        // Umrechnung von Celsius zu Fahrenheit
        return 0.0;
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        // Umrechnung von Fahrenheit zu Celsius
        return 0.0;
    }
}
```

#### Teil 2: Objektorientierte Programmierung (Nachfolgeaufgabe)

**Ziel**: Überführe die Lösung aus Teil 1 in eine objektorientierte Architektur. Erstelle eine Klasse `Temperature`, die Methoden für die Umrechnung beinhaltet. Zusätzlich sollte die Klasse eine Variable für die Temperatur und eine für die Einheit (Celsius oder Fahrenheit) haben.

```java
public class Temperature {
    private double value;
    private String unit;

    // Konstruktor, Getter, Setter, etc.

    public double toCelsius() {
        // Umrechnung zu Celsius
        return 0.0;
    }

    public double toFahrenheit() {
        // Umrechnung zu Fahrenheit
        return 0.0;
    }
}
```

Das Hauptprogramm soll nun ein Objekt dieser Klasse verwenden, um die Umrechnungen durchzuführen.

#### Hinweis

Diese Aufgabe erfordert die Nutzung von Variablen, um Temperaturen und Einheiten zu speichern, mathematischen Operationen für die Umrechnung und Funktionen bzw. Methoden für die Umsetzung der Funktionalität. In Teil 2 wird das Konzept der objektorientierten Programmierung eingeführt, indem die Funktionalität in eine Klasse verlagert wird.
